import { TestBed } from '@angular/core/testing';

import { NgrxAmineAuthService } from './ngrx-amine-auth.service';

describe('NgrxAmineAuthService', () => {
  let service: NgrxAmineAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgrxAmineAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
