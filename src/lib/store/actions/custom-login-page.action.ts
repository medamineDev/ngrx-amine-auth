import {Action} from '@ngrx/store';


export enum CustomAuthActionTypes {
    LOGIN = '[Auth] Login',
    LOGOUT = '[Auth] Logout',
    LOGIN_SUCCESS = '[Auth] Login Success',
    LOGIN_ERROR = '[Auth] Login Error'
}

export class CustomLogInAction implements Action {
    readonly type = CustomAuthActionTypes.LOGIN;

    constructor(public payload: any) {
        console.log(this.type + ' Action library');
        console.log(payload);
    }
}

export class CustomLogOutAction implements Action {
    readonly type = CustomAuthActionTypes.LOGOUT;

    constructor() {
        console.log(this.type + ' Action library');
    }
}


export class CustomLogInSuccessAction implements Action {
    readonly type = CustomAuthActionTypes.LOGIN_SUCCESS;

    constructor(public payload: any) {
        console.log(this.type + ' Action library');
        console.log(payload);
    }
}

export class CustomLogInErrorAction implements Action {
    readonly type = CustomAuthActionTypes.LOGIN_ERROR;

    constructor(public payload: any) {
        console.log(this.type + ' Action library');
        console.log(payload);
    }
}

export type All =
    | CustomLogInErrorAction
    | CustomLogInSuccessAction
    | CustomLogInAction
    | CustomLogOutAction;
