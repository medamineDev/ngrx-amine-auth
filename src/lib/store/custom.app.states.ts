import * as auth from './reducers/login.reducer';
import {createFeatureSelector} from '@ngrx/store';


export interface CustomAppStates {
    authState: auth.CustomState;
}

export const reducers = {
    auth: auth.reducer
};


export const selectAuthState = createFeatureSelector<CustomAppStates>('auth');
