import {NgModule} from '@angular/core';
import {NgrxAmineAuthComponent} from './ngrx-amine-auth.component';
import {NgrxAmineAuthService} from './ngrx-amine-auth.service';

import {EffectsModule} from '@ngrx/effects';
import {CustomAuthEffects} from './store/effects/custom-auth.effects';
import {reducers} from './store/custom.app.states';
import {metaReducers} from './store/reducers/hydration.reducer';
import {CustomAuthActionTypes, All} from './store/actions/custom-login-page.action';
import {StoreModule} from '@ngrx/store';

@NgModule({
    declarations: [NgrxAmineAuthComponent],
    imports: [
        StoreModule.forRoot(reducers, {metaReducers}),
        EffectsModule.forRoot([CustomAuthEffects])

    ],
    providers: [
        NgrxAmineAuthService

    ],
    exports: [StoreModule , EffectsModule]
})
export class NgrxAmineAuthModule {
}
