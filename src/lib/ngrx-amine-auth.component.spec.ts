import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrxAmineAuthComponent } from './ngrx-amine-auth.component';

describe('NgrxAmineAuthComponent', () => {
  let component: NgrxAmineAuthComponent;
  let fixture: ComponentFixture<NgrxAmineAuthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgrxAmineAuthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrxAmineAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
