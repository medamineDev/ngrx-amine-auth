import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class NgrxAmineAuthService {

    public host = 'http://localhost:3000/api/';
    public jsonUrl: './assets/db/users_db.json';
    public isLoggedIn: BehaviorSubject<any>;

    constructor(private httpClient: HttpClient) {
        const token = localStorage.getItem('_token');
        this.isLoggedIn = new BehaviorSubject<boolean>(token != null);
    }

    /**
     * Attempt to login user then fetch user from remote server
     * Returns a Promise<any>
     * @param email
     * @param password
     */

    login(email: string, password: string) {
        console.log('Library inside login service');
        return new Promise(resolve => null);
    }


    /**
     * Attempt to logout user
     * Returns void
     */
    logout() {
    }

}
