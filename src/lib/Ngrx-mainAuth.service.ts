import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {CustomAppStates, selectAuthState} from '../lib/store/custom.app.states';
import {Store} from '@ngrx/store';
import {CustomLogInAction, CustomLogOutAction} from '../lib/store/actions/custom-login-page.action';

@Injectable({
    providedIn: 'root'
})
export class NgrxMainAuthService {

    public authState: Observable<any>;

    constructor(private store: Store<CustomAppStates>) {

        this.authState = this.store.select(selectAuthState);
    }

    login(payload: any) {
        console.log('login Payload from auth Library');
        console.log(payload);
        this.store.dispatch(new CustomLogInAction(payload));
    }

    logout() {
        console.log('logout auth Library');
        this.store.dispatch(new CustomLogOutAction());
    }

    getState() {
        return this.authState;
    }

}
